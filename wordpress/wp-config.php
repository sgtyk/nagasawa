<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'LSh`<CXA`<bnJm_OSu;7.#we/rFk`<:S,v=.=XlPp6uuYiyBB5a]d,RU9vYv_=}Z' );
define( 'SECURE_AUTH_KEY',   'f!RomxhI&_ZD~Y|ki@<hBNA:-TrKD!{/XlAwf,1/M31>jA!v6/c5j .S5av`wPU2' );
define( 'LOGGED_IN_KEY',     '(r5L}d*jzZ}#@.2]=1@dn9:3gk)I6|W6UM;~&;(QWOh#/}5>(_@n@:QFl=RBqEqj' );
define( 'NONCE_KEY',         ')cQF&VI>_Cg%Ts+jwzdqdkIQr+0;1U$2FXGt5*[rx,;f7en@x^BG2`/CITiA{>vT' );
define( 'AUTH_SALT',         'Zz(TQ[wCB?WGTzgFzjq#!133RQQX1hFv@h,YV]j?wXr5vqO34txrMkU,!dXuz|#e' );
define( 'SECURE_AUTH_SALT',  '~ HrhXfxi?Rq5Q9GrUh1v^,begV`X<-CVX$N@.&|fWk:&-$$!m]c|3*a6Dmr@?E+' );
define( 'LOGGED_IN_SALT',    'm(?Ery#RH7W^[J)yMmvusK(xIp`vvJPIRZYGpm@sFdq%/Nt;=RDS^cz[=mK0~hTy' );
define( 'NONCE_SALT',        '%k>|8e~GoCR!Is#qq(41QM6ee::<f3]bAiob$VE1@1wK?q~1;#14DwX`V9d.nY,g' );
define( 'WP_CACHE_KEY_SALT', 'jUg?j^#vtHq0GO0)_c*H2kFL/=;1]q:)Ys66{qY$Hrh^|XF`=xgF3Ri-sp`6=.p9' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'JETPACK_DEV_DEBUG', True );
define( 'WP_DEBUG', True );
define( 'FORCE_SSL_ADMIN', False );
define( 'SAVEQUERIES', False );

// Addtional PHP code in the wp-config.php
// These lines are inserted by VCCW.
// You can place addtional PHP code here!


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
